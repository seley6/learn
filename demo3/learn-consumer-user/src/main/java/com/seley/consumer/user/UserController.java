package com.seley.consumer.user;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class UserController {

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private DiscoveryClient discoveryClient;

	@GetMapping("/getUser")
	public String getUser() {
		List<ServiceInstance> list = discoveryClient.getInstances("service-user");
		if (list.size() > 0) {
			String url = list.get(0).getUri().toString();
			return restTemplate.getForObject(url + "/user/get", String.class);
		} else {
			return null;
		}
	}
}
