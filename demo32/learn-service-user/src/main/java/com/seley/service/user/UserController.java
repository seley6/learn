package com.seley.service.user;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

@RestController
public class UserController {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@GetMapping("/user/get")
	public String getUser() {
		logger.warn("/user/get 被调用了……");
		return "liuyanhui";
	}

	@GetMapping("/user/type1/{id}")
	public String getUserById1(@PathVariable String id) {
		if ("1".equals(id)) {
			return "liuyanhui";
		} else {
			return "anonymous";
		}
	}

	@GetMapping("/user/type2")
	public String getUserById2(@RequestParam("id") String id) {
		if ("1".equals(id)) {
			return "liuyanhui";
		} else {
			return "anonymous";
		}
	}

	@PostMapping("/user/type3")
	public String getUserById3(@RequestParam Map<String, Object> reqMap) {
		String id = (String) reqMap.get("id");
		if ("1".equals(id)) {
			return "liuyanhui";
		} else {
			return "anonymous";
		}
	}
	
	@PostMapping("/user/type4")
	public String getUserById4(@RequestBody String jsonObj) throws Exception {
		Gson gson = new Gson();
		JsonObject obj =  gson.fromJson(jsonObj, JsonObject.class);
		if ("1".equals(obj.get("id").getAsString())) {
			return "liuyanhui";
		} else {
			return "anonymous";
		}
	}
}
