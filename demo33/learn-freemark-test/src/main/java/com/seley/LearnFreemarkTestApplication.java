package com.seley;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LearnFreemarkTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(LearnFreemarkTestApplication.class, args);
	}
}
