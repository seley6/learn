<html>
<head>
<link rel="icon" type="image/x-icon" href="/images/favicon.ico">
<script src="/js/jquery/jquery-1.5.2.min.js" type="text/javascript"></script>

<script>
	$(function() {
		$('#username').keydown(function(e) {
			if (e.keyCode == 13) {
				$("#password").focus();
			}
		});

		$('#password').keydown(function(e) {
			if (e.keyCode == 13) {
				login();
			}
		});
	});

	function login() {
		$.post("/login", {
			username : $("#username").val(),
			password : $("#password").val()
		}, function(data, status) {
			if ('success' === status) {
				if (data) {
					if (data.success == true) {
						location.href = "/home";
						top.document.location.href = "/home";
					} else {
						$("#message").text(data.message);
					}
				} else {
					$("#message").text("系统异常");
				}
			}
		});
	}
</script>

<style type="text/css">
td {
	padding: 5px;
}

html, body {
	margin: 0px;
	height: 100%;
	overflow: hidden;
	background: url("/images/win_bg.jpg") repeat center center;
	background-size: cover;
	color: #444;
	font-family: "微软雅黑";
}

#info {
	width: 100%;
}

#info table{
	padding-right: 130px;
}

.login {
	padding: 30px 30px 30px 30px;
	background-color: #ffffff;
	opacity: 0.65;
	border-radius: 20px;
	width: 280px;
}

#submit {
	color: #000;
	width: 70px;
	border-radius: 5px;
	height: 26px;
}

input {
	background-color: transparent;
	border: 1px solid #000;
}

#username, #password {
	height: 28px;
	width: 180px;
}
</style>
</head>
<body>
	<div id="info">
		<table height="100%" align="right">
			<tr>
				<td valign="middle">
					<div class="login">
						<table>
							<tr>
								<td nowrap="nowrap"><label for="username">用户名：</label></td>
								<td><input id="username" name="username" type="text" /></td>
							</tr>
							<tr>
								<td nowrap="nowrap"><label for="password">密&nbsp;&nbsp;&nbsp;码：</label></td>
								<td><input id="password" name="password" type="password" /></td>
							</tr>
							<tr>
								<td colspan="2" align="right" height="30" valign="top">
									<b id="message" style="color: red; font-size: 10pt;"></b>
									<input type="submit" id="submit" value="登录" onclick="login()" />
								</td>
							</tr>
						</table>
					</div>
				</td>
			</tr>
		</table>
	</div>
</body>
</html>