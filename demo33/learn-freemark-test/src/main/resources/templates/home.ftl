<html>
<head>
<title>${title}</title>

<link rel="icon" type="image/x-icon" href="/images/favicon.ico">

</head>
<body>
	<div id="info">
		用户信息： ${user.name} <br> 用户角色： ${user.role} <br>
		<br>

		<table border=1 cellspacing=0>
			<#list dataList as data> 
				<tr>
					<td>${data.dqn}</td> 
					<td>${data.dqt}</td>
				</tr>
			 </#list>
		</table>
	</div>
</body>
</html>