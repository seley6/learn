package com.seley.consumer.user.feign;

import java.util.Map;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "service-user", fallbackFactory = UserFeignClientFallback.class)
public interface UserFeignClient {

	@GetMapping("/user/get")
	public String getUser();

	@GetMapping("/user/type1/{id}")
	public String getUserById1(@PathVariable(value = "id") String id);

	@GetMapping("/user/type2")
	public String getUserById2(@RequestParam("id") String id);

	@PostMapping("/user/type3")
	public String getUserById3(@RequestParam Map<String, Object> reqMap);

	@PostMapping("/user/type4")
	public String getUserById4(@RequestBody String jsonObj);
}
