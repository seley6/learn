package com.seley.consumer.user.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.seley.consumer.user.feign.UserFeignClient;

@RestController
public class UserController {

	@Autowired
	private UserFeignClient userFeignClient;

	@GetMapping("/getUser")
	public String getUser() {
		return userFeignClient.getUser();
	}
	
	@GetMapping("/getUser1/{id}")
	public String getUserById1(@PathVariable String id) {
		return userFeignClient.getUserById1(id);
	}

	@GetMapping("/getUser2")
	public String getUserById2(@RequestParam("id") String id) {
		return userFeignClient.getUserById2(id);
	}

	@PostMapping("/getUser3")
	public String getUserById3(@RequestParam Map<String, Object> reqMap) {
		return userFeignClient.getUserById3(reqMap);
	}
	
	@PostMapping("/getUser4")
	public String getUserById4(@RequestBody String jsonObj) {
		return userFeignClient.getUserById4(jsonObj);
	}
}
