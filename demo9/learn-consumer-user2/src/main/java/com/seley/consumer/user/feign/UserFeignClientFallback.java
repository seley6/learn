package com.seley.consumer.user.feign;

import java.util.Map;

import org.springframework.stereotype.Component;

import feign.hystrix.FallbackFactory;

@Component
public class UserFeignClientFallback implements FallbackFactory<UserFeignClient> {

	@Override
	public UserFeignClient create(Throwable cause) {
		return new UserFeignClient() {
			
			@Override
			public String getUser() {
				//可把日志记录到其他地方
				System.out.println("失败原因：" + cause.getMessage());
				
				return "调取用户信息异常";
			}
			
			@Override
			public String getUserById1(String id) {
				return "调取用户信息异常";
			}
			
			@Override
			public String getUserById2(String id) {
				return "调取用户信息异常";
			}
			
			@Override
			public String getUserById3(Map<String, Object> reqMap) {
				return "调取用户信息异常";
			}
			
			@Override
			public String getUserById4(String jsonObj) {
				return "调取用户信息异常";
			}
			
		};
	}

	

}
