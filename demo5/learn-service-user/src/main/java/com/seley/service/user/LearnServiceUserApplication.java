package com.seley.service.user;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication
public class LearnServiceUserApplication {

	public static void main(String[] args) {
		SpringApplication.run(LearnServiceUserApplication.class, args);
	}
}
