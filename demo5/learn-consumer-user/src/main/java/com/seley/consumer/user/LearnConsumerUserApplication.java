package com.seley.consumer.user;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class LearnConsumerUserApplication {

	public static void main(String[] args) {
		SpringApplication.run(LearnConsumerUserApplication.class, args);
	}
}
