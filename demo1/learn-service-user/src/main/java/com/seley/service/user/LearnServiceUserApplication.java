package com.seley.service.user;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LearnServiceUserApplication {

	public static void main(String[] args) {
		SpringApplication.run(LearnServiceUserApplication.class, args);
	}
}
