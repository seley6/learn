package com.seley.config.client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LearnConfigClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(LearnConfigClientApplication.class, args);
	}
}
