package com.seley.rabbit.test.exchange;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
public class TopicRecerver {

	@RabbitListener(queues = "topic.tp2.one")
	@RabbitHandler
    public void process(String strs) {
        System.out.println("topic.tp2.one 接收到：" + strs);
    }
	
	@RabbitListener(queues = "topic.tp2.two")
	@RabbitHandler
    public void process2(String strs) {
        System.out.println("topic.tp2.two 接收到：" + strs);
    }
	
}
