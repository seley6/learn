package com.seley.rabbit.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication

public class LearnRabbitTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(LearnRabbitTestApplication.class, args);
	}
	
}
