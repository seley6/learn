package com.seley.rabbit.test.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitTopicConfig {
	
    @Bean
    public TopicExchange topicExchange() {
        return new TopicExchange ("topicExchange");
    }
    
    @Bean
    public Queue topicTp1() {
        return new Queue("topic.tp2.one");
    }
    
    @Bean
    public Queue topicTp2() {
        return new Queue("topic.tp2.two");
    }
    
    @Bean
    public Binding binding1() {
        return BindingBuilder.bind(topicTp1()).to(topicExchange()).with("topic.tp2.*"); //* 后面匹配单个key
    }
    
    @Bean
    public Binding binding2() {
        return BindingBuilder.bind(topicTp2()).to(topicExchange()).with("topic.tp2.#"); //# 后面匹配多个key
    }

}
