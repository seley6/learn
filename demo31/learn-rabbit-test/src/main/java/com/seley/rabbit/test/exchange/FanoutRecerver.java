package com.seley.rabbit.test.exchange;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
public class FanoutRecerver {

	@RabbitListener(queues = "fanout.tp.${server.port}")
	@RabbitHandler
    public void process(String strs) {
        System.out.println("fanout.tp接收到：" + strs);
    }
	
}
