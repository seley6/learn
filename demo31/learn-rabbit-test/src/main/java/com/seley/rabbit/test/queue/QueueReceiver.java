package com.seley.rabbit.test.queue;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
public class QueueReceiver {
	
	@RabbitListener(queues = "queue1")
	@RabbitHandler
    public void process(String strs) {
        System.out.println("接收到：" + strs);
    }
}
