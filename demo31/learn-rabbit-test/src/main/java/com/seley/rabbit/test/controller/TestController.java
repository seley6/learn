package com.seley.rabbit.test.controller;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {
	@Autowired
    private AmqpTemplate rabbitTemplate;
	
	@GetMapping("/queue")
	public String sendQueueMsg(@RequestParam("msg") String msg) {
		rabbitTemplate.convertAndSend("queue1", msg);
		return "success";
	}
	
	@GetMapping("/fanout")
	public String sendFanoutMsg(@RequestParam("msg") String msg) {
		rabbitTemplate.convertAndSend("fanoutExchange","", msg);
		return "success";
	}
	
	@GetMapping("/topic")
	public String sendTopicMsg(@RequestParam("msg") String msg) {
		rabbitTemplate.convertAndSend("topicExchange","topic.tp2.all", msg);
		return "success";
	}
}
