package com.seley.rabbit.test.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitFanoutConfig {
	
	@Value("${server.port}")
	private String port;

    @Bean
    public FanoutExchange fanoutExchange() {
        return new FanoutExchange ("fanoutExchange");
    }
    
    @Bean
    public Queue fanoutTp() {
        return new Queue("fanout.tp."+port);
    }    
    
    @Bean
    public Binding fanoutBinding() {
        return BindingBuilder.bind(fanoutTp()).to(fanoutExchange());
    }

}
