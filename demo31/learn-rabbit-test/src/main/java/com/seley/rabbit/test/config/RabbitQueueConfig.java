package com.seley.rabbit.test.config;

import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitQueueConfig {

    @Bean
    public Queue queue1() {
        return new Queue("queue1", true); //持久化
    }

}
