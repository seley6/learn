package com.seley.gateway.zuul.feign;

import java.util.Map;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "service-login", fallbackFactory = LoginFeignClientFallback.class)
public interface LoginFeignClient {

	@PostMapping("/getToken")
	public Map<String,Object> getToken(@RequestParam("username") String username, @RequestParam("password") String password);

	@PostMapping("/checkToken")
	public boolean validateJWT(@RequestParam("token") String token);
	
	@PostMapping("/reflashToken")
	public String reflashToken(@RequestParam("username") String username, @RequestParam("token") String token);
}
