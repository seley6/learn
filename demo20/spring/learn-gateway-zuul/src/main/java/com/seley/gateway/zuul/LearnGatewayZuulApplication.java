package com.seley.gateway.zuul;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;

import com.seley.gateway.zuul.filter.LoginFilter;

@SpringBootApplication
@EnableZuulProxy
@EnableFeignClients
public class LearnGatewayZuulApplication {

	public static void main(String[] args) {
		SpringApplication.run(LearnGatewayZuulApplication.class, args);
	}
	
	@Bean  
    public LoginFilter loginFilter() {  
        return new LoginFilter();
    }
}
