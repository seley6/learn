package com.seley.gateway.zuul.feign;

import java.util.Map;

import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestParam;

import feign.hystrix.FallbackFactory;

@Component
public class LoginFeignClientFallback implements FallbackFactory<LoginFeignClient> {

	@Override
	public LoginFeignClient create(Throwable cause) {
		return new LoginFeignClient() {

			@Override
			public Map<String,Object> getToken(@RequestParam("username") String username, @RequestParam("password") String password) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public boolean validateJWT(String token) {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public String reflashToken(String username, String token) {
				// TODO Auto-generated method stub
				return null;
			}
			
		};
	}

	

}
