package com.seley;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class LeanrDiscoverEurekaApplication {

	public static void main(String[] args) {
		SpringApplication.run(LeanrDiscoverEurekaApplication.class, args);
	}
}
