import Home from '../views/home.vue';
import Test from '../views/test.vue';

const homeRouters = [
    {
        path: '/home',
        meta: {
            title: 'Home'
        },
        component: Home
    },
    {
        path: '/test',
        component: Test
    }
];

export default homeRouters;