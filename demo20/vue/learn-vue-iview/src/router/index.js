import Vue from 'vue';
import iView from 'iview';
import VueRouter from 'vue-router';
import Util from '../libs/util';
import { tokenMgr } from '../libs/axiosPlugin';

import Index from '../views/index.vue';
import HomeRouters from './home';

Vue.use(VueRouter);

const routers = [
    {
        path: '/',
        component: Index
    },
    ...HomeRouters
];

const vueRouter = new VueRouter({
    mode: 'history',
    routes: routers
});

vueRouter.beforeEach((to, from, next) => {
    if (to.path != '/' && to.path != '/index') {
        //验证token
        tokenMgr.checkToken();
    }

    iView.LoadingBar.start();
    Util.title(to.meta.title);
    next();
});

vueRouter.afterEach((to) => {
    iView.LoadingBar.finish();
    window.scrollTo(0, 0);

    if (to.path != '/' && to.path != '/index') {
        //刷新token
        tokenMgr.reflashToken();
    } else if (to.query.reLogin) {
        iView.Message.info({
            content: '请重新登录',
            duration: 3
        });
    }
});

export default vueRouter;