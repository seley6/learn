import Vue from 'vue';
import iView from 'iview';
import Router from './router';
import Store from './store';
import Axios from './libs/axiosPlugin';
import App from './app.vue';
import 'iview/dist/styles/iview.css';
import jqView from './components';

Vue.use(iView);
Vue.use(Axios);
Vue.use(jqView);

new Vue({
    el: '#app',
    router: Router,
    store: Store,
    render: h => h(App)
});