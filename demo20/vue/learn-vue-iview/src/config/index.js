let config = {
    axiosTimeout: 10000,
    loginUrl: '/api/loginSer/getToken',
    tokenCheckUrl: '/api/loginSer/checkToken',
    tokenReflashUrl: '/api/loginSer/reflashToken'
};

export default config;