let util = {

};

util.title = function(title) {
    title = title ? title : 'Welcome';
    window.document.title = title;
};

export default util;