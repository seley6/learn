import Test1 from './test1';

const jqView = {
    install: function (Vue) {
        Vue.component('Test1', Test1);
    }
};

export default jqView;